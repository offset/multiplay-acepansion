/****** multiplay.acepansion/--background-- *********************************
*
* DESCRIPTION
*  This plugin provides MultiPlay emulation to ACE CPC Emulator.
*
* HISTORY
*
*****************************************************************************
*
*/

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/utility.h>

#include <clib/alib_protos.h>

#define USE_INLINE_API
#include <acepansion/lib_header.h>
#include <libraries/acepansion_plugin.h>

#include "acepansion.h"
#include "interface.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */



struct Library *DOSBase;
struct Library *UtilityBase;



struct PluginDataPort
{
    // Mode
    BOOL isMouse;

    // Joy
    ULONG idx;
    STRPTR preferredName;

    USHORT buttons;
    BYTE ns;
    BYTE ew;
    BYTE lx;
    BYTE ly;
    BYTE rx;
    BYTE ry;

    // Mouse
    LONG sensibility;
};

struct PluginData
{
    // Plugin generic part (do NEVER put anything before this!)
    struct ACEpansionPlugin common;

    // Plugin specifics
    struct SignalSemaphore *sema;

    struct PluginDataPort portA;
    struct PluginDataPort portB;
    STRPTR *list;

    // Host mouse data are common to both ports
    BYTE mouseDeltaX;
    BYTE mouseDeltaY;
    BOOL mouseActive;
    BOOL mouseLeft;
    BOOL mouseRight;
    BOOL mouseMiddle;
};



static UBYTE getJoyStatus(struct PluginDataPort *port);
static VOID setJoyIdx(struct PluginDataPort *port, ULONG selectedId, CONST_STRPTR preferredName);



#define TOOLTYPE_PORT_A "PORTA"
#define TOOLTYPE_PORT_B "PORTB"
#define TOOLTYPE_SENSIBILITY_A "SENSIBILITYA"
#define TOOLTYPE_SENSIBILITY_B "SENSIBILITYB"

#define TOOLTYPE_VALUE_MOUSE "Mouse"



/*
** Private function which is called to initialize commons
** just after the library was loaded into memory and prior
** to any other API call.
**
** This is the good place to open our required libraries.
*/
BOOL InitResources(VOID)
{
    DOSBase = OpenLibrary("dos.library", 0L);
    UtilityBase = OpenLibrary("utility.library", 0L);

    return UtilityBase && DOSBase
        && GUI_InitResources();
}



/*
** Private function which is called to free commons
** just before the library is expurged from memory
**
** This is the good place to close our required libraries.
*/
VOID FreeResources(VOID)
{
    GUI_FreeResources();

    CloseLibrary(UtilityBase);
    CloseLibrary(DOSBase);
}



/****** multiplay.library/CreatePlugin **************************************
*
* NAME
*   CreatePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   struct ACEpansionPlugin * CreatePlugin(CONST_STRPTR *toolTypes, struct SignalSemaphore *sema)
*
* FUNCTION
*   Create the plugin data structure and configure how it will be handled by
*   ACE. This function is usually called at ACE startup.
*
* INPUTS
*   *toolType
*     A pointer to a NULL terminated array of CONST_STRPTR containing the
*     tooltypes provided with the plugin library.
*   *sema
*     Pointer to a semaphore to use to protect access to custom data from
*     ACEpansionPlugin when accessed from subtasks or within your PrefsWindow.
*
* RESULT
*     A pointer to an allocated and initialized plugin data structure.
*
* SEE ALSO
*   DeletePlugin
*   ActivatePlugin
*   GetPrefsPlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
struct ACEpansionPlugin * CreatePlugin(CONST_STRPTR *toolTypes, struct SignalSemaphore *sema)
{
    // We are allocating an extended plugin structure to add our custom stuff
    struct PluginData *myPlugin = (struct PluginData*)AllocVec(sizeof(struct PluginData), MEMF_PUBLIC|MEMF_CLEAR);

    if(myPlugin)
    {
        // Initialize plugin specifics
        myPlugin->sema = sema;
        myPlugin->portA.sensibility = 2;
        myPlugin->portB.sensibility = 2;

        // Get tooltypes to configure ourself
        if(toolTypes) while(*toolTypes)
        {
            CONST_STRPTR ptr = *toolTypes;
            CONST_STRPTR name = ptr;
            ULONG len = 0;

            if(*ptr != '(')
            {
                while(*ptr != '=' && *ptr != '\0')
                {
                    ptr++;
                    len++;
                }

                if(*ptr == '=')
                {
                    if(!Strnicmp(name,TOOLTYPE_PORT_A,len))
                    {
                        CONST_STRPTR value = ptr+1;

                        if(*value != '\0')
                        {
                            if(!(myPlugin->portA.isMouse = !Stricmp(value,TOOLTYPE_VALUE_MOUSE)))
                            {
                                myPlugin->portA.preferredName = StrDup(value);
                            }
                        }
                    }
                    else if(!Strnicmp(name,TOOLTYPE_PORT_B,len))
                    {
                        CONST_STRPTR value = ptr+1;

                        if(*value != '\0')
                        {
                            if(!(myPlugin->portB.isMouse = !Stricmp(value,TOOLTYPE_VALUE_MOUSE)))
                            {
                                myPlugin->portB.preferredName = StrDup(value);
                            }
                        }
                    }
                    else if(!Strnicmp(name,TOOLTYPE_SENSIBILITY_A,len))
                    {
                        LONG sensibility;

                        if(StrToLong(ptr+1,&sensibility) != -1 && sensibility >= 0 && sensibility <= 2)
                        {
                            myPlugin->portA.sensibility = sensibility;
                        }
                    }
                    else if(!Strnicmp(name,TOOLTYPE_SENSIBILITY_B,len))
                    {
                        LONG sensibility;

                        if(StrToLong(ptr+1,&sensibility) != -1 && sensibility >= 0 && sensibility <= 2)
                        {
                            myPlugin->portB.sensibility = sensibility;
                        }
                    }
                }
            }
            toolTypes++;
        }

        // Initialize plugin generic part
        myPlugin->common.ap_APIVersion     = API_VERSION;
        myPlugin->common.ap_Flags          = ACE_FLAGSF_ACTIVE_READIO
                                           | ACE_FLAGSF_SAVE_PREFS
                                           | ACE_FLAGSF_CATCH_HOST_GAMEPAD
                                           | ACE_FLAGSF_CATCH_HOST_MOUSE;
        myPlugin->common.ap_Title          = GetString(MSG_TITLE);
        myPlugin->common.ap_HelpFileName   = LIBNAME".guide";
        myPlugin->common.ap_ToggleMenuName = GetString(MSG_MENU_TOGGLE);
        myPlugin->common.ap_PrefsMenuName  = GetString(MSG_MENU_PREFS);
        myPlugin->common.ap_PrefsWindow    = GUI_Create((struct ACEpansionPlugin *)myPlugin);

        if(myPlugin->common.ap_PrefsWindow)
        {
            GUI_SetModeA(myPlugin->common.ap_PrefsWindow, myPlugin->portA.isMouse);
            GUI_SetModeB(myPlugin->common.ap_PrefsWindow, myPlugin->portB.isMouse);
            GUI_SetMouseSensibilityA(myPlugin->common.ap_PrefsWindow, myPlugin->portA.sensibility);
            GUI_SetMouseSensibilityB(myPlugin->common.ap_PrefsWindow, myPlugin->portB.sensibility);
        }
        else
        {
            DeletePlugin((struct ACEpansionPlugin *)myPlugin);
            myPlugin = NULL;
        }
    }

    return (struct ACEpansionPlugin *)myPlugin;
}



/****** multiplay.library/DeletePlugin **************************************
*
* NAME
*   DeletePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   VOID DeletePlugin(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Delete a plugin data structure. This function is usually called when ACE
*   is exiting.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to delete.
*
* RESULT
*
* SEE ALSO
*   CreatePlugin
*   ActivatePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID DeletePlugin(struct ACEpansionPlugin *plugin)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    FreeVec(myPlugin->portA.preferredName);
    FreeVec(myPlugin->portB.preferredName);
    GUI_Delete(myPlugin->common.ap_PrefsWindow);
    FreeVec(myPlugin);
}



/****** multiplay.library/ActivatePlugin ************************************
*
* NAME
*   ActivatePlugin -- (V1) -- GUI
*
* SYNOPSIS
*   BOOL ActivatePlugin(struct ACEpansionPlugin *plugin, BOOL status)
*
* FUNCTION
*   This function is called everytime ACE wants to activate or disactivate your plugin.
*   While you are disactivated, no other function (except Delete) will the called by ACE.
*   Please note that a plugin is always disactivated by ACE prior to DeletePlugin().
*
* INPUTS
*   activate
*     New activation status requested by ACE.
*
* RESULT
*   Actual activation status (you may fail at activation for some reason, but you
*   should never fail at disactivation request).
*
* SEE ALSO
*   CreatePlugin
*   DeletePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
BOOL ActivatePlugin(UNUSED struct ACEpansionPlugin *plugin, BOOL activate)
{
    return activate;
}



/****** multiplay.acepansion/GetPrefsPlugin *********************************
*
* NAME
*   GetPrefsPlugin -- (V5) -- GUI
*
* SYNOPSIS
*   STRPTR * GetPrefsPlugin(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function that is used by ACE to know about the current preferences of the
*   plugins in order to save them.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   pool
*     Memory pool to use to allocate the tooltypes.
*
* RESULT
*   Pointer to a NULL terminated string array containing the tooltypes or NULL
*   when no tooltypes.
*
* SEE ALSO
*   CreatePlugin
*
*****************************************************************************
*
*/

ACEPANSION_API
STRPTR * GetPrefsPlugin(struct ACEpansionPlugin *plugin, APTR pool)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;
    STRPTR *tooltypes = (STRPTR*)AllocPooled(pool, 5*sizeof(STRPTR));

    if(tooltypes)
    {
        LONG i = 0;

        if(myPlugin->portA.isMouse)
        {
            tooltypes[i++] = MakeToolType(pool, TOOLTYPE_PORT_A, TOOLTYPE_VALUE_MOUSE);
            tooltypes[i++] = MakeToolTypeNum(pool, TOOLTYPE_SENSIBILITY_A, myPlugin->portA.sensibility);
        }
        else if(myPlugin->portA.idx)
        {
            tooltypes[i++] = MakeToolType(pool, TOOLTYPE_PORT_A, myPlugin->list[myPlugin->portA.idx]);
        }

        if(myPlugin->portB.isMouse)
        {
            tooltypes[i++] = MakeToolType(pool, TOOLTYPE_PORT_B, TOOLTYPE_VALUE_MOUSE);
            tooltypes[i++] = MakeToolTypeNum(pool, TOOLTYPE_SENSIBILITY_B, myPlugin->portB.sensibility);
        }
        else if(myPlugin->portB.idx)
        {
            tooltypes[i++] = MakeToolType(pool, TOOLTYPE_PORT_B, myPlugin->list[myPlugin->portB.idx]);
        }    

        tooltypes[i++] = NULL;
    }

    return tooltypes;
}



/****** multiplay.library/Reset *********************************************
*
* NAME
*   Reset -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Reset(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function that is called everytime ACE is issuing a bus reset.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*
* RESULT
*
* SEE ALSO
*   Emulate
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Reset(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** multiplay.library/Emulate *******************************************
*
* NAME
*   Emulate -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Emulate(struct ACEpansionPlugin *plugin, ULONG *signals)
*
* FUNCTION
*   Function that is called at each microsecond of the emulation step inside ACE.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   *signals
*     Pointer to the I/O signals from the expansion port you can use or alter
*     depending on what you are emulating.
*     The signals bits can be decoded using the following flags:
*       ACE_SIGNALF_INT -- (V1)
*         Maskable interrupt; corresponding to /INT signal from CPC expansion
*         port.
*       ACE_SIGNALF_NMI -- (V3)
*         Non-maskable interrupt; corresponding to /NMI signal from CPC
*         expansion port.
*       ACE_SIGNALF_LPEN -- (V4)
*         Light pen trigger; corresponding to /LPEN signal from CPC expansion
*         port.
*       ACE_SIGNALF_BUS_RESET -- (V6)
*         Bus reset trigger; correspond to /BUSRESET signal from CPC expansion
*         port.
*       ACE_SIGNALF_NOT_READY -- (V6)
*         Not ready trigger; correspond to READY signal from CPC expansion port.
*
* RESULT
*
* SEE ALSO
*   Reset
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Emulate(UNUSED struct ACEpansionPlugin *plugin, UNUSED ULONG *signals)
{
}



/****** multiplay.library/WriteIO *******************************************
*
* NAME
*   WriteIO -- (V1) -- EMU
*
* SYNOPSIS
*   VOID WriteIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE value)
*
* FUNCTION
*   Function that is called everytime Z80 is performing a I/O port write operation.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   port
*     Z80 port on with the I/O write operation was issued.
*   value
*     Value that was issued on the port.
*
* RESULT
*
* SEE ALSO
*   ReadIO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID WriteIO(UNUSED struct ACEpansionPlugin *plugin, UNUSED USHORT port, UNUSED UBYTE value)
{
}



/****** multiplay.library/ReadIO ********************************************
*
* NAME
*   ReadIO -- (V1) -- EMU
*
* SYNOPSIS
*   VOID ReadIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE *value)
*
* FUNCTION
*   Function that is called everytime Z80 is performing a I/O port read operation.
*   Note: Implement this function only if is it really required for your plugin because it
*   is obviously CPU intensive.
*
* INPUTS
*   port
*     Z80 port on with the I/O read operation was issued.
*   *value
*     Value to be returned.
*
* RESULT
*
* SEE ALSO
*   WriteIO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReadIO(struct ACEpansionPlugin *plugin, USHORT port, UBYTE *value)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if((port & 0xfef8) == 0xf890)
    {
        switch(port & 7)
        {
            // Port A Action bits
            case 0:
                if(myPlugin->portA.isMouse)
                {
                    *value = (myPlugin->mouseLeft   ? 0x10 : 0x00)
                           | (myPlugin->mouseRight  ? 0x20 : 0x00)
                           | (myPlugin->mouseMiddle ? 0x40 : 0x00);
                }
                else
                {
                    *value = getJoyStatus(&myPlugin->portA);
                }
                break;
            // Port B Action bits
            case 1:
                if(myPlugin->portB.isMouse)
                {
                    *value = (myPlugin->mouseLeft   ? 0x10 : 0x00)
                           | (myPlugin->mouseRight  ? 0x20 : 0x00)
                           | (myPlugin->mouseMiddle ? 0x40 : 0x00);
                }
                else
                {
                    *value = getJoyStatus(&myPlugin->portB);
                }
                break;
            // Port A Mouse POS-X value
            case 2:
                if(myPlugin->portA.isMouse)
                {
                    *value = myPlugin->mouseDeltaX;
                    myPlugin->mouseDeltaX = 0;
                }
                break;
            // Port A Mouse POS-Y value
            case 3:
                if(myPlugin->portA.isMouse)
                {
                    *value = myPlugin->mouseDeltaY;
                    myPlugin->mouseDeltaY = 0;
                }
                break;
            // Port B Mouse POS-X value
            case 4:
                if(myPlugin->portB.isMouse)
                {
                    *value = myPlugin->mouseDeltaX;
                    myPlugin->mouseDeltaX = 0;
                }
                break;
            // Port B Mouse POS-Y value
            case 5:
                if(myPlugin->portB.isMouse)
                {
                    *value = myPlugin->mouseDeltaY;
                    myPlugin->mouseDeltaY = 0;
                }
                break;
        }
    }
}



/****** multiplat.acepansion/WriteMem ***************************************

* NAME
*  WriteMem -- (V6) -- EMU
*
* SYNOPSIS
*  BOOL WriteMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE value)
*
* FUNCTION
*  Function that is called everytime Z80 is performing a memory write
*  operation.
*
* INPUTS
*  *plugin
*    Pointer to the plugin data structure to use.
*  address
*    Z80 address on which the memory write operation was issued.
*  value
*    Value that has to be written.
*
* RESULT
*  TRUE if the plugins actually cougth the memory write operation so that
*  internal memory will be left unchanged (/RAMDIS is set to 0).
*
* NOTES
*  Implement this function only if is it really required for your plugin
*  because it is obviously CPU intensive.
*
* SEE ALSO
*  ReadMem()
*
*****************************************************************************
*
*/

ACEPANSION_API
BOOL WriteMem(UNUSED struct ACEpansionPlugin *plugin, UNUSED USHORT address, UNUSED UBYTE value)
{
    return FALSE;
}



/****** multiplay.acepansion/ReadMem ****************************************

* NAME
*  ReadMem -- (V6) -- EMU
*
* SYNOPSIS
*  BOOL ReadMem(struct ACEpansionPlugin *plugin, USHORT address, UBYTE *value, BOOL opcode)

* FUNCTION
*  Function that is called everytime Z80 is performing a memory read
*  operation.
*
* INPUTS
*  *plugin
*    Pointer to the plugin data structure to use.
*  address
*    Z80 address on which memory read operation was issued.
*  *value
*    Value to be returned.
*  opcode
*    Boolean which is TRUE when the memory read operation is related to
*    Z80 opcode fetching (/M1 signal). If FALSE, then this is a regular
*    read operation during opcode execution.
*
* RESULT
*  TRUE if the plugins actually cougth the memory read operation so that
*  internal memory won't be read (both /ROMDIS and /RAMDIS are set to 0).
*
* NOTES
*  Implement this function only if is it really required for your plugin
*  because it is obviously CPU intensive.
*
* SEE ALSO
*  WriteMem()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReadMem(UNUSED struct ACEpansionPlugin *plugin, UNUSED USHORT address, UNUSED UBYTE *value, UNUSED BOOL opcodeFetch)
{
}



/****** multiplay.library/GetAudio ******************************************
*
* NAME
*   GetAudio -- (V1) -- EMU
*
* SYNOPSIS
*   VOID GetAudio(struct ACEpansionPlugin *plugin, SHORT *leftSample, SHORT *rightSample)
*
* FUNCTION
*   Function to be used when your plugin needs to mix an audio output with the one
*   from ACE.
*   It is called at each PSG emulation step, which means at 125KHz.
*
* INPUTS
*   *leftSample
*     Pointer to store the left audio sample to be mixed with ACE audio.
*   *rightSample
*     Pointer to store the right audio sample to be mixed with ACE audio.
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID GetAudio(UNUSED struct ACEpansionPlugin *plugin, UNUSED SHORT *leftSample, UNUSED SHORT *rightSample)
{
}



/****** multiplay.library/Printer *******************************************
*
* NAME
*   Printer -- (V1) -- EMU
*
* SYNOPSIS
*   VOID WritePrinter(struct ACEpansionPlugin *plugin, UBYTE data, BOOL strobe, BOOL *busy)
*
* FUNCTION
*   Function that is called everytime the CPC is accessing the printer port.
*
* INPUTS
*   data
*     8 bits written value (note that CPC is limited to 7 bits values, only Amstrad Plus
*     can actually provide the 8th bit).
*   strobe
*     Status of the /STROBE signal from the printer port.
*   *busy
*     Busy signal value that can be altered.
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Printer(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE data, UNUSED BOOL strobe, UNUSED BOOL *busy)
{
}



/****** multiplay.library/Joystick ******************************************
*
* NAME
*   Joystick -- (V2) -- EMU
*
* SYNOPSIS
*   VOID Joystick(struct ACEpansionPlugin *plugin, BOOL com1, BOOL com2, UBYTE *ioData)
*
* FUNCTION
*   Function which is called everytime the CPC is accessing the joystick port.
*   Please note that the joystick port in R/W but you cannot know if the CPC
*   is actually reading or writing to it.
*
* INPUTS
*   com1
*     If com1 is active then the CPC is requesting access to joystick 0.
*   com2
*     If com2 is active then the CPC is requesting access to joystick 1.
*   *ioData
*     If the CPC is reading the joystick port (regular case) ioData will be set
*     to 0xff and you are supposed to set to 0 the bits corresponding to the
*     requested joystick (you should use the ACE_IODATAF_JOYSTICK_XXX defines).
*     Please note the additional ACE_IODATAF_JOYSTICK_PAUSE which is not
*     existing on CPC but that you could use to map the play/pause button of
*     a joystick used in ACE on the 'P' key of the keyboard (or the GX4000
*     pause button).
*     If the CPC is writing the joystick port the ioData will contain the
*     written value to the port and modifying it will have not effect (on a
*     real CPC it could simply destroy the PSG chipset!).
*
* RESULT
    To emulate the analog joystick port of the Amstrad Plus and the GX-4000,
    this function should be used together with AnalogInput().
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Joystick(UNUSED struct ACEpansionPlugin *plugin, UNUSED BOOL com1, UNUSED BOOL com2, UNUSED UBYTE *ioData)
{
}



/****** multiplay.acepansion/AnalogInput ************************************
*
* NAME
*   AnalogInput -- (V7) -- EMU
*
* SYNOPSIS
*   VOID AnalogInput(struct ACEpansionPlugin *plugin, UBYTE channel, UBYTE *value)
*
* FUNCTION
*   Function which is called everytime the CPC is accessing the analog joystick
*   port of the Amstrad Plus or the GX-4000.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   channel
*     Analog channel number (from 0 to 3).
*   *value
*     Pointer where to store the 6-bit wide value of the data on the channel.
*
* RESULT
*
* NOTES
*   To emulate the analog joystick port of the Amstrad Plus and the GX-4000,
*   this function should be used together with Joystick() which is providing
*   fire buttons (they are shared between both analog and digital joystick
*   ports).
*
* SEE ALSO
*   Joystick()
*****************************************************************************
*
*/

ACEPANSION_API
VOID AnalogInput(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE channel, UNUSED UBYTE *value)
{
}



/****** multiplay.library/AcknowledgeInterrupt ******************************
*
* NAME
*   AcknowledgeInterrupt -- (V1) -- EMU
*
* SYNOPSIS
*   VOID AcknowledgeInterrupt(struct ACEpansionPlugin *plugin, UBYTE *ivr)
*
* FUNCTION
*   Function which is called everytime the Z80 is entering an interrupt.
*
* INPUTS
*   *ivr
*     IVR could be set to the desired value (Interrupt Vector Register).
*
* RESULT
*
* SEE ALSO
*   ReturnInterrupt
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID AcknowledgeInterrupt(UNUSED struct ACEpansionPlugin *plugin, UNUSED UBYTE *ivr)
{
}



/****** multiplay.acepansion/ReturnInterrupt ********************************
*
* NAME
*   ReturnInterrupt -- (V3) -- EMU
*
* SYNOPSIS
*   VOID ReturnInterrupt(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function which is called everytime the Z80 is returning from an interrupt
*   using RETI instruction.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*   AcknowledgeInterrupt
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID ReturnInterrupt(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** multiplay.acepansion/Cursor *****************************************
*
* NAME
*   Cursor -- (V1) -- EMU
*
* SYNOPSIS
*   VOID Cursor(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   Function which is called everytime the cursor signal from the CRTC is
*   active.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID Cursor(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** multiplay.acepansion/HostGamepadList ********************************
*
* NAME
*   HostGamepadList -- (V7) -- GUI

* SYNOPSIS
*   VOID HostGamepadList(struct ACEpansionPlugin *plugin, STRPTR *list)
*
* FUNCTION
*   This function is called everytime a gamepad in plugged or unplugged from
*   the host so that the plugin can know about the available devices. Index
*   from HostGamepadEvent() will refer to this list.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   list
*     NULL terminated array of strings containing the human readable names of
*     the plugged gamepads, the first one being the localized string for "No
*     joystick". Please note that the contents of this list remains valid
*     until the function is invoked again, so that you can use it safely in
*     your code without duplication.
*
* RESULT
*
* NOTES
*
* SEE ALSO
*   HostGamepadList()
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostGamepadList(struct ACEpansionPlugin *plugin, STRPTR *list)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;
    ULONG idx;

    myPlugin->list = list;

    myPlugin->portA.idx = 0;
    myPlugin->portB.idx = 0;

    for(idx=1; list[idx] != NULL; idx++)
    {
        // Check if we are matching preferences
        if((myPlugin->portA.idx == 0 && idx == 1) // Default to 1st found joytick
        || (myPlugin->portA.preferredName && Stricmp(myPlugin->portA.preferredName, list[idx]) == 0))
        {
            myPlugin->portA.idx = idx;
        }
        if((myPlugin->portB.idx == 0 && idx == 2) // Default to 2nd found joytick
        || (myPlugin->portB.preferredName && Stricmp(myPlugin->portB.preferredName, list[idx]) == 0))
        {
            myPlugin->portB.idx = idx;
        }
    }

    GUI_SetJoystickList(myPlugin->common.ap_PrefsWindow, myPlugin->list, myPlugin->portA.idx, myPlugin->portB.idx);
}



/****** multiplay.acepansion/HostGamepadEvent *******************************
*
* NAME
*   HostGamepadEvent -- (V7) -- GUI
*
* SYNOPSIS
*   VOID HostGamepadEvent(struct ACEpansionPlugin *plugin, UBYTE index, USHORT buttons, BYTE ns, BYTE ew, BYTE lx, BYTE ly, BYTE rx, BYTE ry)
*
* FUNCTION
*   This function is called everytime the state of a host's gamepad state is
*   changing. It let you handle both analogic and digital devices.
*
* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   index
*     Index of the gamepad in gamepad list.
*   buttons
*     State of the gamepad buttons.
*     Buttons bits can be decoded using the ACE_HOSTGAMEPADF_BUTTON_XXX flags.
*   ns
*     Horizontal position of directional buttons from -128 to +127.
*   ew
*     Vertical position of directional buttons from -128 to +127.
*   lx
*     Horizontal position of left stick from -128 to +127.
*   ly
*     Vertical position of left stick from -128 to +127.
*   rx
*     Horizontal position of right stick from -128 to +127.
*   ry
*     Vertical position of right stick from -128 to +127.
*
* RESULT
*
* NOTES
*
* SEE ALSO
*   HostGamepadList()
*   libraries/acepansion_plugin.h
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostGamepadEvent(struct ACEpansionPlugin *plugin, UBYTE index, USHORT buttons, BYTE ns, BYTE ew, BYTE lx, BYTE ly, BYTE rx, BYTE ry)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    if(index == myPlugin->portA.idx)
    {
        myPlugin->portA.buttons = buttons;
        myPlugin->portA.ns = ns;
        myPlugin->portA.ew = ew;
        myPlugin->portA.lx = lx;
        myPlugin->portA.ly = ly;
        myPlugin->portA.rx = rx;
        myPlugin->portA.ry = ry;
    }
    if(index == myPlugin->portB.idx)
    {
        myPlugin->portB.buttons = buttons;
        myPlugin->portB.ns = ns;
        myPlugin->portB.ew = ew;
        myPlugin->portB.lx = lx;
        myPlugin->portB.ly = ly;
        myPlugin->portB.rx = rx;
        myPlugin->portB.ry = ry;
    }
}



/****** multiplay.acepansion/HostMouseEvent *********************************
*
* NAME
*   HostMouseEvent -- (V7) -- GUI

* SYNOPSIS
*   VOID HostMouseEvent(struct ACEpansionPlugin *plugin, UBYTE buttons, SHORT deltaX, SHORT deltaY)

* FUNCTION
*   Function that is called everytime a host's mouse event occured

* INPUTS
*   *plugin
*     Pointer to the plugin data structure to use.
*   buttons
*     State of the mouse buttons.
*     The mouse buttons bits can be decoded using the following flags:
*       ACE_HOSTMOUSEF_BUTTON_MAIN
*         State of main mouse button.
*       ACE_HOSTMOUSEF_BUTTON_SECOND
*         State of second mouse button.
*       ACE_HOSTMOUSEF_BUTTON_MIDDLE
*         State of middle mouse button.
*   deltaX
*     Horizontal mouse move until previous event.
*   deltaY
*     Vertical mouse move until previous event.
*
* RESULT
*
* NOTES
*
* SEE ALSO
*   libraries/acepansion_plugin.h
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostMouseEvent(struct ACEpansionPlugin *plugin, UBYTE buttons, SHORT deltaX, SHORT deltaY)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    myPlugin->mouseLeft   = (buttons & ACE_HOSTMOUSEF_BUTTON_MAIN)   ? TRUE : FALSE;
    myPlugin->mouseRight  = (buttons & ACE_HOSTMOUSEF_BUTTON_SECOND) ? TRUE : FALSE;
    myPlugin->mouseMiddle = (buttons & ACE_HOSTMOUSEF_BUTTON_MIDDLE) ? TRUE : FALSE;

    // Limit to range -8..+7
    if(myPlugin->mouseDeltaX < 7 && myPlugin->mouseDeltaX > -8)
    {
        myPlugin->mouseDeltaX += deltaX >> (2 - myPlugin->portA.sensibility);

        if(myPlugin->mouseDeltaX > 7) myPlugin->mouseDeltaX = 7;
        if(myPlugin->mouseDeltaX < -8) myPlugin->mouseDeltaX = -8;
    }
    // Limit to range -8..+7
    if(myPlugin->mouseDeltaY < 7 && myPlugin->mouseDeltaY > -8)
    {
        myPlugin->mouseDeltaY -= deltaY >> (2 - myPlugin->portB.sensibility);

        if(myPlugin->mouseDeltaY > 7) myPlugin->mouseDeltaY = 7;
        if(myPlugin->mouseDeltaY < -8) myPlugin->mouseDeltaY = -8;
    }
}



/****** multiplay.acepansion/HostLightDeviceDiodePulse **********************
*
* NAME
*   HostLightDeviceDiodePulse -- (V7) -- EMU
*
* SYNOPSIS
*   VOID HostLightDeviceDiodePulse(struct ACEpansionPlugin *plugin)
*
* FUNCTION
*   This function is called at each photo-diode pulse, when the beam from
*   the screen monitor is just in front of the light device photo-diode.
*
* INPUTS
*
* RESULT
*
* SEE ALSO
*   HostLightDeviceButton
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostLightDeviceDiodePulse(UNUSED struct ACEpansionPlugin *plugin)
{
}



/****** multiplay.acepansion/HostLightDeviceButton **************************
*
* NAME
*   HostLightDeviceButton -- (V7) -- GUI
*
* SYNOPSIS
*   VOID HostLightDeviceButton(struct ACEpansionPlugin *plugin, BOOL pressed)
*
* FUNCTION
*   This function is called everytime the state of the light device button
*   is changing.
*
* INPUTS
*   pressed
*     New state of the light device button.
*
* RESULT
*
* NOTE
*   When a light device plugin is activated, ACE is mapping this button on
*   mouse main button, so that on-screen click will emulate light device
*   button.
*
* SEE ALSO
*   HostLightDeviceDiodePulse
*
*****************************************************************************
*
*/

ACEPANSION_API
VOID HostLightDeviceButton(UNUSED struct ACEpansionPlugin *plugin, UNUSED BOOL pressed)
{
}



/*
** Joysticks configuration handling
*/

static UBYTE getJoyStatus(struct PluginDataPort *port)
{
    UBYTE value = 0x00;

    // Buttons mapping
    if(port->buttons & ACE_HOSTGAMEPADF_BUTTON_BOTTOM)
        value |= 0x10;
    if(port->buttons & ACE_HOSTGAMEPADF_BUTTON_LEFT)
        value |= 0x20;
    if(port->buttons & ACE_HOSTGAMEPADF_BUTTON_TOP)
        value |= 0x40;
    // East-West, upper rear left/right, lx and rx will do left/right
    if(port->buttons & ACE_HOSTGAMEPADF_BUTTON_UPPER_REAR_RIGHT
    || port->ew > +32 || port->lx > +32 || port->rx > +32)
        value |= 0x08;
    if(port->buttons & ACE_HOSTGAMEPADF_BUTTON_UPPER_REAR_LEFT
    || port->ew < -32 || port->lx < -32 || port->rx < -32)
        value |= 0x04;
    // North-South, lower rear left/right, ly and ry will do up/down
    if(port->buttons & ACE_HOSTGAMEPADF_BUTTON_LOWER_REAR_RIGHT
    || port->ns > +32 || port->ly > +32 || port->ry > +32)
        value |= 0x02;
    // Also add several buttons mapping for up direction to jump easily in some games
    if(port->buttons & (ACE_HOSTGAMEPADF_BUTTON_LOWER_REAR_LEFT|ACE_HOSTGAMEPADF_BUTTON_RIGHT|ACE_HOSTGAMEPADF_BUTTON_LEFT_STICK|ACE_HOSTGAMEPADF_BUTTON_RIGHT_STICK)
    || port->ns < -32 || port->ly < -32 || port->ry < -32)
        value |= 0x01;

    return value;
}

static VOID setJoyIdx(struct PluginDataPort *port, ULONG selectedId, CONST_STRPTR preferredName)
{
    port->idx = selectedId;
    FreeVec(port->preferredName);

    if(selectedId)
        port->preferredName = StrDup(preferredName);
    else
        port->preferredName = NULL;
}



/*
** Functions called from the GUI
*/
VOID Plugin_SetModeA(struct ACEpansionPlugin *plugin, BOOL isMouse)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    ObtainSemaphore(myPlugin->sema);
    myPlugin->portA.isMouse = isMouse;
    ReleaseSemaphore(myPlugin->sema);
}

VOID Plugin_SetModeB(struct ACEpansionPlugin *plugin, BOOL isMouse)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    ObtainSemaphore(myPlugin->sema);
    myPlugin->portB.isMouse = isMouse;
    ReleaseSemaphore(myPlugin->sema);
}

VOID Plugin_SetMouseSensibilityA(struct ACEpansionPlugin *plugin, LONG sensibility)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    ObtainSemaphore(myPlugin->sema);
    myPlugin->portA.sensibility = sensibility;
    ReleaseSemaphore(myPlugin->sema);
}

VOID Plugin_SetMouseSensibilityB(struct ACEpansionPlugin *plugin, LONG sensibility)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    ObtainSemaphore(myPlugin->sema);
    myPlugin->portB.sensibility = sensibility;
    ReleaseSemaphore(myPlugin->sema);
}

VOID Plugin_SetJoystickA(struct ACEpansionPlugin *plugin, ULONG selectedId)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    ObtainSemaphore(myPlugin->sema);
    setJoyIdx(&myPlugin->portA, selectedId, myPlugin->list[selectedId]);
    ReleaseSemaphore(myPlugin->sema);
}

VOID Plugin_SetJoystickB(struct ACEpansionPlugin *plugin, ULONG selectedId)
{
    struct PluginData *myPlugin = (struct PluginData *)plugin;

    ObtainSemaphore(myPlugin->sema);
    setJoyIdx(&myPlugin->portB, selectedId, myPlugin->list[selectedId]);
    ReleaseSemaphore(myPlugin->sema);
}

