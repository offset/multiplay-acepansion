/*
** multiplay.acepansion public API
*/

#ifndef ACEPANSION_H
#define ACEPANSION_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#ifndef ACEPANSION_PLUGIN_H
#include <acepansion/plugin.h>
#endif


#define LIBNAME "multiplay.acepansion"
#define VERSION 2
#define REVISION 1
#define DATE "21.10.2023"
#define COPYRIGHT "� 2021-2023 Philippe Rimauro"

#define API_VERSION 7


/*
** Joystick plugin specific API to be called from GUI
*/
VOID Plugin_SetModeA(struct ACEpansionPlugin *plugin, BOOL isMouse);
VOID Plugin_SetModeB(struct ACEpansionPlugin *plugin, BOOL isMouse);
VOID Plugin_SetMouseSensibilityA(struct ACEpansionPlugin *plugin, LONG sensibility);
VOID Plugin_SetMouseSensibilityB(struct ACEpansionPlugin *plugin, LONG sensibility);
VOID Plugin_SetJoystickA(struct ACEpansionPlugin *plugin, ULONG selectedId);
VOID Plugin_SetJoystickB(struct ACEpansionPlugin *plugin, ULONG selectedId);


#endif /* ACEPANSION_H */

