## Version $VER: multiplay.acepansion.catalog 2.0 (14.07.2022)
## Languages english fran�ais deutsch espa�ol
## Codeset english 0
## Codeset fran�ais 0
## Codeset deutsch 0
## Codeset espa�ol 0
## SimpleCatConfig CharsPerLine 200
## Header Locale_Strings
## TARGET C english "generated/locale_strings.h" NoCode
## TARGET CATALOG fran�ais "Release/Catalogs/fran�ais/" Optimize
## TARGET CATALOG deutsch "Release/Catalogs/deutsch/" Optimize
## TARGET CATALOG espa�ol "Release/Catalogs/espa�ol/" Optimize
MSG_MENU_TOGGLE
Plug a MultiPlay
Brancher une MultiPlay
MultiPlay anschlie�en
Conecte un MultiPlay
;
MSG_MENU_PREFS
MultiPlay configuration...
Configuration de la MultiPlay...
MultiPlay-Konfiguration ...
Configuraci�n del MultiPlay...
;
MSG_TITLE
MultiPlay
MultiPlay
MultiPlay
MultiPlay
;
MSG_GROUP_PORT_A
Assignment of port A
Affectation du port A
Belegung des Anschlusses A
Asignaci�n del puerto A
;
MSG_GROUP_PORT_B
Assignment of port B
Affectation du port B
Belegung des Anschlusses B
Asignaci�n del puerto B
;
MSG_CYCLE_BUBBLE_HELP
Configure connected mice and joysticks.
Configuration des souris et manettes de jeu connect�es.
Konfiguriere angeschlossene M�use und Steuerkn�ppel.
Configuraci�n de los ratones y joysticks conectados.
;
MSG_CHECK_USE_MOUSE
Use a mouse
Utiliser une souris
Benutze eine Maus
Usar un rat�n
;
MSG_LABEL_SENSIBILITY
Sensibility:
Sensibilit� :
Empfindlichkeit:
Sensibilidad:
;
