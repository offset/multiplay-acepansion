/*
** Graphical User Interface generic header
**
** interface_mui.c
**      Implementation using the Magic User Interface
**      for MorphOS, AmigaOS PPC, AmigaOS 68K and AROS
*/

#ifndef ACEPANSION_GUI_H
#define ACEPANSION_GUI_H

#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif
#ifndef INTUITION_CLASSUSR_H
#include <intuition/classusr.h>
#endif
#ifndef ACEPANSION_PLUGIN_H
#include <acepansion/plugin.h>
#endif


/*
** GUI generic API
*/
BOOL GUI_InitResources(VOID);
VOID GUI_FreeResources(VOID);

Object * GUI_Create(struct ACEpansionPlugin *plugin);
VOID GUI_Delete(Object *gui);


/*
** MultiPlay GUI specific API to be called from the plugin
*/
VOID GUI_SetModeA(Object *gui, BOOL mouseMode);
VOID GUI_SetModeB(Object *gui, BOOL mouseMode);
VOID GUI_SetMouseSensibilityA(Object *gui, LONG sensibility);
VOID GUI_SetMouseSensibilityB(Object *gui, LONG sensibility);
VOID GUI_SetJoystickList(Object *gui, STRPTR *list, ULONG selectedIdA, ULONG selectedIdB);


#endif /* ACEPANSION_GUI_H */

