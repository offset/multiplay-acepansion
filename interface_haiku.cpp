/*
** Graphical User Interface using the Be API
** for MultiPlay ACEpansion
*/

#include <Box.h>
#include <CheckBox.h>
#include <GroupView.h>
#include <LayoutBuilder.h>
#include <OptionPopUp.h>
#include <Slider.h>

#include <acepansion/lib_header.h>

#include "acepansion.h"
#include "interface.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */

class JoyMouseGroup: public BBox
{
    public:
    JoyMouseGroup(STRPTR title, struct ACEpansionPlugin* plugin);

    void AttachedToWindow() override;
    void MessageReceived(BMessage* override);

    BOptionPopUp* CYA_Select;
    BCheckBox *CHK_UseMouse;
    BSlider* NUM_Mouse;
    BCardLayout* PGE_Port;

    private:

    struct ACEpansionPlugin* myPlugin;
};

JoyMouseGroup::JoyMouseGroup(STRPTR title, struct ACEpansionPlugin* plugin)
        : BBox("JoyMouseGroup")
        , myPlugin(plugin)
{
    SetLabel(title);

    BGroupView* gv = new BGroupView(B_HORIZONTAL);
	AddChild(gv);
    gv->GroupLayout()->SetSpacing(B_USE_SMALL_SPACING);

    BLayoutBuilder::Group<>(gv)
        .SetInsets(B_USE_DEFAULT_SPACING)
        .Add(CHK_UseMouse = new BCheckBox("UseMouse", GetString(MSG_CHECK_USE_MOUSE), new BMessage('UMSE')))
        .AddCards()
            .GetLayout(&PGE_Port)
            .Add(CYA_Select = new BOptionPopUp("Joy", NULL, new BMessage('SJOY')))
            .Add(NUM_Mouse = new BSlider("Mouse", GetString(MSG_LABEL_SENSIBILITY),
                new BMessage('SENS'), 0, 2, B_HORIZONTAL))
        .End()
    .End();

	PGE_Port->SetVisibleItem(0);

    CYA_Select->SetExplicitMinSize(BSize(StringWidth("W") * 15, B_SIZE_UNSET));
}

void JoyMouseGroup::AttachedToWindow()
{
    CHK_UseMouse->SetTarget(this);
    CYA_Select->SetTarget(this);
    NUM_Mouse->SetTarget(this);
}

void JoyMouseGroup::MessageReceived(BMessage* message)
{
    switch (message->what)
    {
        case 'UMSE':
		{
            int32 isMouse = message->FindInt32("be:value");
			if (this == Parent()->ChildAt(0))
            	Plugin_SetModeA(myPlugin, isMouse);
			else
            	Plugin_SetModeB(myPlugin, isMouse);
            PGE_Port->SetVisibleItem(isMouse);
            break;
		}
        case 'SENS':
		{
			if (this == Parent()->ChildAt(0))
            	Plugin_SetMouseSensibilityA(myPlugin, message->FindInt32("be:value"));
			else
            	Plugin_SetMouseSensibilityB(myPlugin, message->FindInt32("be:value"));
            break;
		}
        case 'SJOY':
        {
            // Notify joystick selection
            int32 v = message->FindInt32("be:value");
			if (this == Parent()->ChildAt(0))
				Plugin_SetJoystickA(myPlugin, v);
			else
				Plugin_SetJoystickB(myPlugin, v);
            break;
        }
        default:
            BBox::MessageReceived(message);
            break;
    }
}

/*
** Function which is called to initialize commons just
** after the library was loaded into memory and prior
** to any other API call
**
** This is the good place to open our required libraries
** and create our MUI custom classes
*/
BOOL GUI_InitResources(VOID)
{
	return TRUE;
}

/*
** Function which is called to free commons just before
** the library is expurged from memory
**
** This is the good place to close our required libraries
** and destroy our MUI custom classes
*/
VOID GUI_FreeResources(VOID)
{
}




/*
** Function which is called from CreatePlugin() to build the GUI
*/
Object * GUI_Create(struct ACEpansionPlugin *plugin)
{
	BGroupView* group = new BGroupView(B_VERTICAL);
	group->AddChild(new JoyMouseGroup(GetString(MSG_GROUP_PORT_A), plugin));
	group->AddChild(new JoyMouseGroup(GetString(MSG_GROUP_PORT_B), plugin));
	return (Object*)group;
}


/*
** Function which is called from DeletePlugin() to delete the GUI
*/
VOID GUI_Delete(Object *gui)
{
	BView* view = (BView*)gui;
	BWindow* window = view->Window();
	window->Lock();
	view->RemoveSelf();
	window->Unlock();
	delete view;
}



/*
** Functions called from the plugin
*/
VOID GUI_SetModeA(Object *gui, BOOL isMouse)
{
	BGroupView* top = (BGroupView*)gui;
	JoyMouseGroup* port = (JoyMouseGroup*)top->ChildAt(0);
	port->CHK_UseMouse->SetValue(isMouse);
	port->PGE_Port->SetVisibleItem(isMouse);
}

VOID GUI_SetModeB(Object *gui, BOOL isMouse)
{
	BGroupView* top = (BGroupView*)gui;
	JoyMouseGroup* port = (JoyMouseGroup*)top->ChildAt(1);
	port->CHK_UseMouse->SetValue(isMouse);
	port->PGE_Port->SetVisibleItem(isMouse);
}

VOID GUI_SetMouseSensibilityA(Object *gui, LONG sensibility)
{
	BGroupView* top = (BGroupView*)gui;
	JoyMouseGroup* port = (JoyMouseGroup*)top->ChildAt(0);
	port->NUM_Mouse->SetValue(sensibility);
}

VOID GUI_SetMouseSensibilityB(Object *gui, LONG sensibility)
{
	BGroupView* top = (BGroupView*)gui;
	JoyMouseGroup* port = (JoyMouseGroup*)top->ChildAt(1);
	port->NUM_Mouse->SetValue(sensibility);
}



VOID GUI_SetJoystickList(Object *gui, STRPTR *list, ULONG selectedIdA, ULONG selectedIdB)
{
	BGroupView* top = (BGroupView*)gui;
	top->LockLooper();

	JoyMouseGroup* portA = (JoyMouseGroup*)top->ChildAt(0);
	JoyMouseGroup* portB = (JoyMouseGroup*)top->ChildAt(1);
    while (portA->CYA_Select->CountOptions())
        portA->CYA_Select->RemoveOptionAt(0);
    while (portB->CYA_Select->CountOptions())
        portB->CYA_Select->RemoveOptionAt(0);

    for (int i = 0; list[i]; i++) {
        portA->CYA_Select->AddOptionAt(list[i], i, i);
        portB->CYA_Select->AddOptionAt(list[i], i, i);
    }

    portA->CYA_Select->SetValue(selectedIdA);
    portB->CYA_Select->SetValue(selectedIdB);

	top->UnlockLooper();
}


