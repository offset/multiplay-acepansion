/*
** Graphical User Interface using MUI
** for MultiPlay ACEpansion
*/

#include <clib/alib_protos.h>

#include <proto/exec.h>
#include <proto/utility.h>
#include <proto/intuition.h>
#include <proto/muimaster.h>

#include <libraries/asl.h>
#include <acepansion/lib_header.h>

#include "acepansion.h"
#include "interface.h"
#define CATCOMP_NUMBERS
#include "generated/locale_strings.h" /* catalog string ids */



struct Library *IntuitionBase;
struct Library *UtilityBase;
struct Library *MUIMasterBase;



struct MUI_CustomClass *MCC_MultiPlayClass = NULL;



/*
** MCC_MultiPlayClass definitions
*/
#define MultiPlayObject NewObject(MCC_MultiPlayClass->mcc_Class, NULL

#define MUIA_MultiPlay_Plugin (TAG_USER | 0x0000) // [I..] struct ACEpansionPlugin *, Reference to the plugin managing the GUI (mandatory)

#define MUIM_MultiPlay_Notify (TAG_USER | 0x0100) // struct MUIP_MultiPlay_Notify, notify the plugin or the GUI about changes
    #define MUIV_MultiPlay_Notify_ToGUI    0 // Notify the GUI
    #define MUIV_MultiPlay_Notify_ToPlugin 1 // Notify the plugin
        // For both directions
        #define MUIV_MultiPlay_Notify_ModeA                10 // Notify about joystick/mouse mode in port A
        #define MUIV_MultiPlay_Notify_ModeB                11 // Notify about joystick/mouse mode in port B
        #define MUIV_MultiPlay_Notify_MouseSensibilityA    12 // Notify about mouse sensibility for port A
        #define MUIV_MultiPlay_Notify_MouseSensibilityB    13 // Notify about mouse sensibility for port B
        // For MUIV_MultiPlay_Notify_ToGUI only
        #define MUIV_MultiPlay_Notify_JoystickList         14 // Notify about MultiPlay list and indexes
        // For MUIV_MultiPlay_Notify_ToPlugin only
        #define MUIV_MultiPlay_Notify_JoystickSelectedIdA  15 // Notify about selected id for joystick on port A
        #define MUIV_MultiPlay_Notify_JoystickSelectedIdB  16 // Notify about selected id for joystick on port B

struct MUIP_MultiPlay_Notify
{
    IPTR id;
    IPTR direction;
    IPTR type;
    union
    {
        // Type = MUIV_MultiPlay_Notify_ModeA
        //        MUIV_MultiPlay_Notify_ModeB
        IPTR isMouse;
        // Type = MUIV_MultiPlay_Notify_MouseSensibility
        IPTR mouseSensibility;
        // Type = MUIV_MultiPlay_Notify_JoystickList
        struct
        {
            CONST_STRPTR *joystickList;
            IPTR joystickSelectedIdA;
            IPTR joystickSelectedIdB;
        };
        // Type = MUIV_MultiPlay_Notify_JoystickSelectedIdA
        //        MUIV_MultiPlay_Notify_JoystickSelectedIdB
        IPTR joystickSelectedId;
    };
};

struct MultiPlayData
{
    struct ACEpansionPlugin *plugin;

    Object *PGE_PortA;
    Object *PGE_PortB;

    Object *CHK_ModeA;
    Object *CYA_JoystickSelectA;
    Object *NUM_MouseSensibilityA;

    Object *CHK_ModeB;
    Object *CYA_JoystickSelectB;
    Object *NUM_MouseSensibilityB;
};



/*
** Helper functions
*/
static Object * makeJoyMouseGroup(STRPTR title, Object **PGE_Port, Object **CHK_Mode, Object **NUM_MouseSensibility, Object **CYA_Joy)
{
    static CONST_STRPTR dummy[] = { "...", NULL };

    Object *obj = VGroup,
        GroupFrameT(title),
        MUIA_ShortHelp, GetString(MSG_CYCLE_BUBBLE_HELP),
        Child, HGroup,
            Child, *CHK_Mode = MUI_MakeObject(MUIO_Checkmark,0),
            Child, LLabel1(GetString(MSG_CHECK_USE_MOUSE)),
            Child, HVSpace,
            MUIA_CycleChain, TRUE,
            End,
        Child, *PGE_Port = HGroup,
            MUIA_Group_PageMode, TRUE,
            Child, *CYA_Joy = CycleObject,
                MUIA_Cycle_Entries, dummy,
                MUIA_Cycle_Active, 0,
                MUIA_CycleChain, TRUE,
                End,
            Child, HGroup,
                Child, Label(GetString(MSG_LABEL_SENSIBILITY)),
                Child, *NUM_MouseSensibility = SliderObject,
                    MUIA_Slider_Min, 0,
                    MUIA_Slider_Max, 2,
                    MUIA_CycleChain, TRUE,
                    End,
                End,
            End,
        End;

    return obj;
}


/*
** MCC_MultiPlayClass methods
*/
static IPTR mNotify(struct IClass *cl, Object *obj, struct MUIP_MultiPlay_Notify *msg)
{
    struct MultiPlayData *data = INST_DATA(cl,obj);

    switch(msg->direction)
    {
        case MUIV_MultiPlay_Notify_ToGUI:
            switch(msg->type)
            {
                case MUIV_MultiPlay_Notify_ModeA:
                    nnset(data->CHK_ModeA, MUIA_Selected, msg->isMouse);
                    set(data->PGE_PortA, MUIA_Group_ActivePage, msg->isMouse ? 1 : 0);
                    break;
                case MUIV_MultiPlay_Notify_ModeB:
                    nnset(data->CHK_ModeB, MUIA_Selected, msg->isMouse);
                    set(data->PGE_PortB, MUIA_Group_ActivePage, msg->isMouse ? 1 : 0);
                    break;
                case MUIV_MultiPlay_Notify_MouseSensibilityA:
                    nnset(data->NUM_MouseSensibilityA, MUIA_Slider_Level, msg->mouseSensibility);
                    break;
                case MUIV_MultiPlay_Notify_MouseSensibilityB:
                    nnset(data->NUM_MouseSensibilityB, MUIA_Slider_Level, msg->mouseSensibility);
                    break;
                case MUIV_MultiPlay_Notify_JoystickList:
                    // Update list
                    nnset(data->CYA_JoystickSelectA, MUIA_Cycle_Entries, msg->joystickList);
                    nnset(data->CYA_JoystickSelectB, MUIA_Cycle_Entries, msg->joystickList);
                    // Update indexes
                    nnset(data->CYA_JoystickSelectA, MUIA_Cycle_Active, msg->joystickSelectedIdA);
                    nnset(data->CYA_JoystickSelectB, MUIA_Cycle_Active, msg->joystickSelectedIdB);
                    break;
            }
            break;

        case MUIV_MultiPlay_Notify_ToPlugin:
            switch(msg->type)
            {
                case MUIV_MultiPlay_Notify_ModeA:
                    Plugin_SetModeA(data->plugin, msg->isMouse);
                    nnset(data->PGE_PortA, MUIA_Group_ActivePage, msg->isMouse ? 1 : 0);
                    break;
                case MUIV_MultiPlay_Notify_ModeB:
                    Plugin_SetModeB(data->plugin, msg->isMouse);
                    nnset(data->PGE_PortB, MUIA_Group_ActivePage, msg->isMouse ? 1 : 0);
                    break;
                case MUIV_MultiPlay_Notify_MouseSensibilityA:
                    Plugin_SetMouseSensibilityA(data->plugin, msg->mouseSensibility);
                    break;
                case MUIV_MultiPlay_Notify_MouseSensibilityB:
                    Plugin_SetMouseSensibilityB(data->plugin, msg->mouseSensibility);
                    break;
                case MUIV_MultiPlay_Notify_JoystickSelectedIdA:
                    Plugin_SetJoystickA(data->plugin, msg->joystickSelectedId);
                    break;
                case MUIV_MultiPlay_Notify_JoystickSelectedIdB:
                    Plugin_SetJoystickB(data->plugin, msg->joystickSelectedId);
                    break;
            }
            break;
    }
    return 0;
}

static IPTR mNew(struct IClass *cl, Object *obj, Msg msg)
{

    struct TagItem *tags,*tag;
    struct MultiPlayData *data;

    struct ACEpansionPlugin *plugin = NULL;

    Object *PGE_PortA;
    Object *PGE_PortB;

    Object *CHK_ModeA;
    Object *NUM_MouseSensibilityA;
    Object *CYA_JoystickSelectA;

    Object *CHK_ModeB;
    Object *NUM_MouseSensibilityB;
    Object *CYA_JoystickSelectB;

    // Parse initial taglist
    for(tags=((struct opSet *)msg)->ops_AttrList; (tag=NextTagItem(&tags)); )
    {
        switch(tag->ti_Tag)
        {
            case MUIA_MultiPlay_Plugin:
                plugin = (struct ACEpansionPlugin *)tag->ti_Data;
                break;                              
        }
    }

    if(!plugin) return 0;

    obj = (Object *)DoSuperNew(cl,obj,
        Child, makeJoyMouseGroup(GetString(MSG_GROUP_PORT_A), &PGE_PortA, &CHK_ModeA, &NUM_MouseSensibilityA, &CYA_JoystickSelectA),
        Child, makeJoyMouseGroup(GetString(MSG_GROUP_PORT_B), &PGE_PortB, &CHK_ModeB, &NUM_MouseSensibilityB, &CYA_JoystickSelectB),
        TAG_MORE,((struct opSet *)msg)->ops_AttrList);

    if(!obj) return 0;

    data = INST_DATA(cl,obj);

    data->plugin = plugin;

    data->PGE_PortA = PGE_PortA;
    data->PGE_PortB = PGE_PortB;

    data->CHK_ModeA = CHK_ModeA;
    data->NUM_MouseSensibilityA = NUM_MouseSensibilityA;
    data->CYA_JoystickSelectA = CYA_JoystickSelectA;

    data->CHK_ModeB = CHK_ModeB;
    data->NUM_MouseSensibilityB = NUM_MouseSensibilityB;
    data->CYA_JoystickSelectB = CYA_JoystickSelectB;

    // Mouse set port mode
    DoMethod(CHK_ModeA, MUIM_Notify, MUIA_Selected, MUIV_EveryTime,
             obj, 4, MUIM_MultiPlay_Notify, MUIV_MultiPlay_Notify_ToPlugin,
                     MUIV_MultiPlay_Notify_ModeA, MUIV_TriggerValue);
    DoMethod(CHK_ModeB, MUIM_Notify, MUIA_Selected, MUIV_EveryTime,
             obj, 4, MUIM_MultiPlay_Notify, MUIV_MultiPlay_Notify_ToPlugin,
                     MUIV_MultiPlay_Notify_ModeB, MUIV_TriggerValue);
    // Mouse sensitivity
    DoMethod(NUM_MouseSensibilityA, MUIM_Notify, MUIA_Slider_Level, MUIV_EveryTime,
             obj, 4, MUIM_MultiPlay_Notify, MUIV_MultiPlay_Notify_ToPlugin,
                     MUIV_MultiPlay_Notify_MouseSensibilityA, MUIV_TriggerValue);
    DoMethod(NUM_MouseSensibilityB, MUIM_Notify, MUIA_Slider_Level, MUIV_EveryTime,
             obj, 4, MUIM_MultiPlay_Notify, MUIV_MultiPlay_Notify_ToPlugin,
                     MUIV_MultiPlay_Notify_MouseSensibilityB, MUIV_TriggerValue);
    // Joystick select
    DoMethod(CYA_JoystickSelectA, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime,
             obj, 4, MUIM_MultiPlay_Notify, MUIV_MultiPlay_Notify_ToPlugin,
                     MUIV_MultiPlay_Notify_JoystickSelectedIdA, MUIV_TriggerValue);
    DoMethod(CYA_JoystickSelectB, MUIM_Notify, MUIA_Cycle_Active, MUIV_EveryTime,
             obj, 4, MUIM_MultiPlay_Notify, MUIV_MultiPlay_Notify_ToPlugin,
                     MUIV_MultiPlay_Notify_JoystickSelectedIdB, MUIV_TriggerValue);

    return (IPTR)obj;
}



/*
** MUI Custom Class dispatcher, creation & destruction
*/
DISPATCHER(MultiPlayClass)
{
    switch (msg->MethodID)
    {
        case OM_NEW                : return(mNew   (cl,obj,(APTR)msg));
        case MUIM_MultiPlay_Notify : return(mNotify(cl,obj,(APTR)msg));
    }
}
DISPATCHER_END



/*
** Function which is called to initialize commons just
** after the library was loaded into memory and prior
** to any other API call
**
** This is the good place to open our required libraries
** and create our MUI custom classes
*/
BOOL GUI_InitResources(VOID)
{
    UtilityBase = OpenLibrary("utility.library", 0L);
    IntuitionBase = OpenLibrary("intuition.library", 0L);
    MUIMasterBase = OpenLibrary("muimaster.library", 0L);

    if(UtilityBase && IntuitionBase && MUIMasterBase)
    {
        MCC_MultiPlayClass = MUI_CreateCustomClass(
            NULL,
            MUIC_Group,
            NULL,
            sizeof(struct MultiPlayData),
            DISPATCHER_REF(MultiPlayClass));
    }

    return MCC_MultiPlayClass != NULL;
}

/*
** Function which is called to free commons just before
** the library is expurged from memory
**
** This is the good place to close our required libraries
** and destroy our MUI custom classes
*/
VOID GUI_FreeResources(VOID)
{
    if(MCC_MultiPlayClass)
        MUI_DeleteCustomClass(MCC_MultiPlayClass);

    CloseLibrary(MUIMasterBase);
    CloseLibrary(IntuitionBase);
    CloseLibrary(UtilityBase);
}



/*
** Function which is called from CreatePlugin() to build the GUI
*/
Object * GUI_Create(struct ACEpansionPlugin *plugin)
{
    return MultiPlayObject,
        MUIA_MultiPlay_Plugin, plugin,
        End;
}

/*
** Function which is called from DeletePlugin() to delete the GUI
*/
VOID GUI_Delete(Object *gui)
{
    MUI_DisposeObject(gui);
}



/*
** Functions called from the plugin
*/
VOID GUI_SetModeA(Object *gui, BOOL isMouse)
{
    DoMethod(gui, MUIM_MultiPlay_Notify, MUIV_MultiPlay_Notify_ToGUI,
             MUIV_MultiPlay_Notify_ModeA, isMouse);
}

VOID GUI_SetModeB(Object *gui, BOOL isMouse)
{
    DoMethod(gui, MUIM_MultiPlay_Notify, MUIV_MultiPlay_Notify_ToGUI,
             MUIV_MultiPlay_Notify_ModeB, isMouse);
}

VOID GUI_SetMouseSensibilityA(Object *gui, LONG sensibility)
{
    DoMethod(gui, MUIM_MultiPlay_Notify, MUIV_MultiPlay_Notify_ToGUI,
             MUIV_MultiPlay_Notify_MouseSensibilityA, sensibility);
}

VOID GUI_SetMouseSensibilityB(Object *gui, LONG sensibility)
{
    DoMethod(gui, MUIM_MultiPlay_Notify, MUIV_MultiPlay_Notify_ToGUI,
             MUIV_MultiPlay_Notify_MouseSensibilityB, sensibility);
}

VOID GUI_SetJoystickList(Object *gui, STRPTR *list, ULONG selectedIdA, ULONG selectedIdB)
{
    DoMethod(gui, MUIM_MultiPlay_Notify, MUIV_MultiPlay_Notify_ToGUI,
             MUIV_MultiPlay_Notify_JoystickList, list, selectedIdA, selectedIdB);
}

